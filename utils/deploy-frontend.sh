#!/bin/bash

PACKAGE_VERSION=$(cat package.json | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[", ]//g')

DEST=/Users/razvans/Projects/romania-pe-bune/deployments/rpb-research-frontend-test/
rm -rf $DEST/*
rsync -a dist/* $DEST
cd $DEST
git pull
git status
git add .
git commit -m "version $PACKAGE_VERSION"
git tag "v$PACKAGE_VERSION"
git push origin master --tags
