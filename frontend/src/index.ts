const css = require('./styles.css');

import { Logger } from './logger';
import { authSvc, UserRoles } from './auth';
import { adminHandler } from './handle-admin';
import { notvalidHandler } from './handle-notvalidated';
import { researchHandler } from './handle-research';
import { API_BASE_URL, WEB_BASE_URL } from './constants';


// Document elements
const signInSection = document.getElementById('signin-section');
const signoutButton = document.getElementById('signoutButton');

async function main() {
  try {
    const googleCode = detectGoogleCallback();
    if (!googleCode) {
      authSvc.load();
      if (!authSvc.isAuthenticated) {
        signInSection.style.display = 'block';
        setGoogleSignInHref();
      } else {
        if (signoutButton) {
          signoutButton.addEventListener('click', async () => {
            await signOut();
          });
        }

        if (authSvc.isWaitingValidation) {
          notvalidHandler.setupUI();
        }

        if (authSvc.isResearcher) {
          researchHandler.setupUI();
        }

        if (authSvc.isAdmin || authSvc.isCoordinator) {
          adminHandler.setupUI();
        }
      }
    } else {
      Logger.debug('google is calling', googleCode);
      await signIn(googleCode);
      window.location.replace(WEB_BASE_URL);
    }
  } catch (ex) {
    Logger.debug(ex);
  }
}

function detectGoogleCallback(): string | null {
  const params = new URLSearchParams(document.location.search);
  const state = params.get('state');
  if (!!state) {
    const parts = state.split('=');
    if (parts.length === 2 && parts[0] === 'provider' && parts[1] === 'google') {
      const code = params.get('code');
      return code;
    }
  }

  return null;
}

async function setGoogleSignInHref() {
  try {
    const linkEl = document.getElementById('googleSignIn') as HTMLAnchorElement;
    const authUrl = await getGoogleAuthUrl();
    Logger.debug(authUrl);
    linkEl.href = authUrl;
  } catch (ex) {
    Logger.debug(ex);
  }
}

async function signIn(code: string): Promise<void> {
  const opts: RequestInit = {
    method: 'GET',
    cache: 'no-cache',
    mode: 'cors',
    redirect: 'follow',
    headers: {
      'Content-Type': 'application/json'
    }
  };
  const url = `${API_BASE_URL}/auth/google/signin?code=${code}&redirect_uri=${WEB_BASE_URL}`;
  const resp = await fetch(url, opts)
    .then((response) => response.json());
  Logger.debug(resp);
  authSvc.init(resp.user, resp.token);
}

async function signOut(): Promise<void> {
  const opts: RequestInit = {
    method: 'GET',
    cache: 'no-cache',
    mode: 'cors',
    redirect: 'follow',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': authSvc.authHeader
    }
  };
  const url = `${API_BASE_URL}/auth/signout`;
  await fetch(url, opts)
    .then((resp) => {
      Logger.debug(resp);
      if (resp.status === 200) {
        authSvc.reset();
        window.location.replace(WEB_BASE_URL);
      }
    })
    .catch((error) => {
      Logger.error(error);
    });
}

async function getGoogleAuthUrl(): Promise<string> {
  const opts: RequestInit = {
    method: 'GET',
    cache: 'no-cache',
    mode: 'cors',
    redirect: 'follow',
    headers: {
      'Content-Type': 'application/json'
    }
  };
  const url = `${API_BASE_URL}/auth/google/auth-url?redirect_uri=${WEB_BASE_URL}`;
  const resp = await fetch(url, opts)
    .then((response) => response.json());
  return resp.authUrl;
}

main();