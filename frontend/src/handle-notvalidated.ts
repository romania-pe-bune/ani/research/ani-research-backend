import { authSvc } from './auth';
import { Logger } from './logger';



class NotValidateHandler {
  private signoutSection: HTMLElement;
  private section: HTMLElement;
  private title: HTMLElement;

  public async setupUI(): Promise<void> {
    this.section = document.getElementById('waiting-validation-section');
    this.section.style.display = 'block';

    this.signoutSection = document.getElementById('signout-section');
    this.signoutSection.style.display = 'block';

    const user = authSvc.currentUser;
    this.title = document.getElementById('not-validated-title');
    this.title.innerHTML = `<h1>Hi, ${user.displayName} [${user.email}]</h1>`;
  }
}

export const notvalidHandler = new NotValidateHandler();