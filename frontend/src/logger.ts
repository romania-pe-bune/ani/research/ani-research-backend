const ENABLED = true;

const noop = () => { };

export class Logger {
  static get debug() {
    if (ENABLED) {
      return console.debug.bind(console);
    }
    return noop;
  }

  static get error() {
    if (ENABLED) {
      return console.error.bind(console);
    }
    return noop;
  }

  static get log() {
    if (ENABLED) {
      return console.log.bind(console);
    }
    return noop;
  }

  static get info() {
    if (ENABLED) {
      return console.info.bind(console);
    }
    return noop;
  }

  static get warn() {
    if (ENABLED) {
      return console.warn.bind(console);
    }
    return noop;
  }
}
