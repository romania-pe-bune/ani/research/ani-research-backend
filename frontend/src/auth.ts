
export enum UserRole {
  // eslint-disable-next-line no-unused-vars
  researcher = 10,
  // eslint-disable-next-line no-unused-vars
  reviewer = 70,
  // eslint-disable-next-line no-unused-vars
  coordinator = 150,
  // eslint-disable-next-line no-unused-vars
  admin = 250
}

export type UserRoles = UserRole[];

interface IAuthToken {
  access: string;
  refresh: string;
  accessExpiresIn: number;
  accessExpiresAt: number;
  refreshExpiresIn: number;
  refreshExpiresAt: number;
}

export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  displayName: string;
  email: string;
  profileImageUrl: string;
  roles: UserRole[];
  sessionId: string;
  status: number;
}

const USER_ITEM = 'rpb-user';
const TOKEN_ITEM = 'rpb-token';

class Auth {
  private user: IUser;
  private token: IAuthToken;

  init(user?: IUser, token?: IAuthToken): void {
    console.log(user);
    if (user) {
      this.user = user;
    }
    if (token) {
      this.token = token;
      this.token.accessExpiresAt = new Date().getTime() + this.token.accessExpiresIn * 1000;
      this.token.refreshExpiresAt = new Date().getTime() + this.token.refreshExpiresIn * 1000;
    }
    this.saveUser();
    this.saveToken();
  }

  load() {
    this.loadUser();
    this.loadToken();
  }

  reset(): void {
    this.user = null;
    this.token = null;
    this.saveUser();
    this.saveToken();
  }

  get isAuthenticated(): boolean {
    if (!this.user || !this.token) {
      return false;
    }

    const now = new Date().getTime();
    return now < this.token.accessExpiresAt;
  }

  get currentUser(): IUser {
    return this.user;
  }

  get isAdmin(): boolean {
    return this.user?.roles.indexOf(UserRole.admin) >= 0;
  }

  get isCoordinator(): boolean {
    return this.user?.roles.indexOf(UserRole.coordinator) >= 0;
  }

  get isReviewer(): boolean {
    return this.user?.roles.indexOf(UserRole.reviewer) >= 0;
  }

  get isResearcher(): boolean {
    return this.user?.roles.indexOf(UserRole.researcher) >= 0;
  }

  get isWaitingValidation(): boolean {
    return this.user?.roles.length === 0;
  }

  get authHeader(): string {
    if (this.isAuthenticated) {
      return `Bearer ${this.token.access}`;
    }
    return '';
  }

  getRolesString(roles: UserRole[]): string {
    return roles
      .map((r) => this.getRoleName(r))
      .join(', ');
  }

  getRoleName(role: UserRole): string {
    switch (role) {
      case UserRole.admin: return 'admin';
      case UserRole.coordinator: return 'coordinator';
      case UserRole.reviewer: return 'reviewer';
      case UserRole.researcher: return 'researcher';
    }
  }

  private saveUser(): void {
    localStorage.setItem(USER_ITEM, JSON.stringify(this.user));
  }

  private loadUser(): void {
    const item = localStorage.getItem(USER_ITEM);
    if (item) {
      this.user = JSON.parse(item);
    }
  }

  private saveToken(): void {
    localStorage.setItem(TOKEN_ITEM, JSON.stringify(this.token));
  }

  private loadToken(): void {
    const item = localStorage.getItem(TOKEN_ITEM);
    if (item) {
      this.token = JSON.parse(item);
    }
  }
}

export const authSvc = new Auth();