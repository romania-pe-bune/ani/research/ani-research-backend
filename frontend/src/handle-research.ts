import { authSvc } from "./auth";



class ResearchHandler {
  private signoutSection: HTMLElement;
  private section: HTMLElement;
  private title: HTMLElement;

  public async setupUI(): Promise<void> {
    this.section = document.getElementById('researcher-section');
    this.section.style.display = 'block';
    if (!authSvc.isAuthenticated) {
      return;
    }

    this.signoutSection = document.getElementById('signout-section');
    this.signoutSection.style.display = 'block';
  }
}

export const researchHandler = new ResearchHandler();