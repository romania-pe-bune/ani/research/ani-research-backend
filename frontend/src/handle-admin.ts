import { format } from 'date-fns';
import { Logger } from './logger';
import { authSvc, UserRole, UserRoles } from './auth';
import { API_BASE_URL } from './constants';


export interface IProviderData {
  [key: string]: number | string | Date | object | null;
}

interface IUser {
  id: number;
  email: string;
  firstName?: string;
  lastName?: string;
  displayName: string;
  phone?: string;
  roleId: UserRole;
  role: string;
  roles?: UserRoles;
  provider: string;
  providerData?: IProviderData;
  lastLogin: Date;
  created: Date;
  updated: Date;
}

class AdminHandler {
  private adminSection: HTMLElement;
  private signoutSection: HTMLElement;
  private selectedUser: IUser;

  public async setupUI(): Promise<void> {
    this.adminSection = document.getElementById('admin-section');
    this.signoutSection = document.getElementById('signout-section');
    this.adminSection.style.display = 'block';
    this.signoutSection.style.display = 'block';
    const users = await this.getUsers();
    Logger.debug(users);
    const rowContainer = document.getElementById('users-body');
    users
      .sort((a, b) => a.displayName.localeCompare(b.displayName))
      .forEach((u, idx) => {
        const btnEditId = `btn-user-edit-${idx}`;
        const btnDeleteId = `btn-user-delete-${idx}`;

        const r = document.createElement('div');
        r.className = 'users-row';
        r.innerHTML = `
        <div class="users-rowid">${idx + 1}</div>
        <div class="users-name">${u.displayName}</div>
        <div class="users-email">${u.email}</div>
        <div class="users-roles">${u.role}</div>
        <div class="users-created">${this.formatDate(u.created)}</div>
        <div class="users-lastlogin">${this.formatDate(u.lastLogin)}</div>
        <div class="users-actions">
        <button id="${btnEditId}" class="users-action-btn"><span class="material-icons">edit</span></button>
        <button id="${btnDeleteId}" class="users-action-btn"><span class="material-icons">delete</span></button>
        </div>
        `;
        rowContainer.appendChild(r);

        const btnEdit = document.getElementById(btnEditId);
        btnEdit.addEventListener('click', () => {
          this.onEditBtnClick(u);
        });
        const btnDel = document.getElementById(btnDeleteId);
        btnDel.addEventListener('click', () => {
          this.onDeleteBtnClick(u);
        });
      });

    document.getElementById('updateUserButton').addEventListener('click', this.onUpdateBtnClick.bind(this));
  }

  private async onEditBtnClick(user: IUser) {
    // Logger.debug('edit', user);
    this.selectedUser = await this.getUserDetails(user.id);
    const detailsEl = document.getElementById('user-details');
    const uidEl = document.getElementById('user-details-uid') as HTMLInputElement;
    const displayEl: HTMLInputElement = document.getElementById('user-display-text') as HTMLInputElement;
    const emailEl = document.getElementById('user-details-email');

    uidEl.value = this.selectedUser.id.toString();
    displayEl.value = this.selectedUser.displayName;
    emailEl.innerText = this.selectedUser.email;

    document.getElementsByName('user-roles').forEach((el) => {
      (el as HTMLInputElement).checked = false;
    });
    this.selectedUser.roles.forEach((role) => {
      const id = `user-roles-${role}`;
      (document.getElementById(id) as HTMLInputElement).checked = true;
    });

    detailsEl.style.display = 'flex';
  }

  private async onUpdateBtnClick() {
    Logger.debug('update');
    try {
      if (!authSvc.isAuthenticated || !this.selectedUser) {
        return;
      }

      const uidEl = document.getElementById('user-details-uid') as HTMLInputElement;
      const displayEl: HTMLInputElement = document.getElementById('user-display-text') as HTMLInputElement;
      // const data: { id: number; displayName: string; roles: UserRoles } = {
      //   id: parseInt(uidEl.value, 10),
      //   displayName: displayEl.value,
      //   roles: []
      // };
      this.selectedUser.displayName = displayEl.value
      this.selectedUser.roles = [];

      document.getElementsByName('user-roles').forEach((el: HTMLInputElement) => {
        if (el.checked) {
          this.selectedUser.roles.push(parseInt(el.value, 10));
        }
      });
      Logger.debug(this.selectedUser);

      const opts: RequestInit = {
        method: 'PUT',
        cache: 'no-cache',
        mode: 'cors',
        redirect: 'follow',
        headers: {
          'Authorization': authSvc.authHeader,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          firstName: this.selectedUser.firstName,
          lastName: this.selectedUser.lastName,
          displayName: this.selectedUser.displayName,
          phone: this.selectedUser.phone || '',
          roles: this.selectedUser.roles
        })
      };
      const url = `${API_BASE_URL}/users/${this.selectedUser.id}`;
      const resp = await fetch(url, opts)
        .then((r) => r.json());
      if (resp.status === 200) {
        location.reload();
      } else {
        console.log(resp);
      }
    } catch (ex) {
      Logger.log(ex);
    }
  }

  private async onDeleteBtnClick(user: IUser) {
    // Logger.debug('delete', user);
    try {
      if (!authSvc.isAuthenticated || !user) {
        return;
      }

      const opts: RequestInit = {
        method: 'DELETE',
        cache: 'no-cache',
        mode: 'cors',
        redirect: 'follow',
        headers: {
          'Authorization': authSvc.authHeader
        }
      };
      const url = `${API_BASE_URL}/users/${user.id}`;
      const resp = await fetch(url, opts);
      if (resp.status === 200) {
        location.reload();
      }
    } catch (ex) {
      Logger.log(ex);
    }
  }

  private async getUserDetails(userId: number): Promise<IUser> {
    try {
      if (!authSvc.isAuthenticated) {
        return;
      }

      const opts: RequestInit = {
        method: 'GET',
        cache: 'no-cache',
        mode: 'cors',
        redirect: 'follow',
        headers: {
          'Authorization': authSvc.authHeader
        }
      };
      const u = await fetch(`${API_BASE_URL}/users/${userId}`, opts)
        .then((response) => response.json());
      return {
        ...u,
        created: u.created ? new Date(Date.parse(u.created)) : null,
        updated: u.updated ? new Date(Date.parse(u.updated)) : null,
        lastLogin: u.lastLogin ? new Date(Date.parse(u.lastLogin)) : null
      };
    } catch (ex) {
      Logger.log(ex);
      return null;
    }
  }

  private async getUsers(): Promise<IUser[]> {
    try {
      if (!authSvc.isAuthenticated) {
        return;
      }

      const opts: RequestInit = {
        method: 'GET',
        cache: 'no-cache',
        mode: 'cors',
        redirect: 'follow',
        headers: {
          'Authorization': authSvc.authHeader
        }
      };
      const resp = await fetch(`${API_BASE_URL}/users`, opts)
        .then((response) => response.json());
      return resp.map((u: any) => {
        return {
          ...u,
          created: u.created ? new Date(Date.parse(u.created)) : null,
          updated: u.updated ? new Date(Date.parse(u.updated)) : null,
          lastLogin: u.lastLogin ? new Date(Date.parse(u.lastLogin)) : null
        }
      });
    } catch (ex) {
      Logger.log(ex);
      return [];
    }
    return [];
  }

  private formatDate(date: Date): string {
    if (!date) {
      return '';
    }
    return format(date, 'dd/MM/yyyy HH:mm');
  }
}

export const adminHandler = new AdminHandler();