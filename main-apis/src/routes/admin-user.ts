import { Router } from 'express';
import { AdminUserController } from '~controllers';
import { adminUserPolicy } from '~policies';

// Init shared
// eslint-disable-next-line new-cap
const router = Router();
const ctrl = new AdminUserController();

// /api/users
router.route('/')
  .all(adminUserPolicy.isAllowed.bind(adminUserPolicy))
  .get(ctrl.list.bind(ctrl));

router.route('/:auserId')
  .all(adminUserPolicy.isAllowed.bind(adminUserPolicy))
  .get(ctrl.read.bind(ctrl))
  .put(ctrl.update.bind(ctrl))
  .delete(ctrl.delete.bind(ctrl));

router.param('auserId', ctrl.userById);

export default router;
