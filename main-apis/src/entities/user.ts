import { UserRole } from '~shared';

/* eslint-disable no-unused-vars */
export enum UserStatus {
  pending = 0,
  active = 1,
  idle = 2,
  closed = 3,
  deleted = 4,
  blackListed = 5
}
/* eslint-enable no-unused-vars */

export interface IProviderData {
  [key: string]: number | string | Date | object | null;
}

export interface IUserFull {
  id: number;
  firstName: string;
  lastName: string;
  displayName: string;
  email: string;
  phone: string;
  roles: UserRole[];
  provider: string;
  providerData: IProviderData;
  profileImageUrl: string;
  googleId: string;
  created: Date;
  updated: Date;
  status: UserStatus;
  sessionId: string;
  settings: any;
}

export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  displayName: string;
  created: Date;
  email: string;
  status: UserStatus;
  roles: string[];
  settings: any;
  profileImageUrl: string;
}

export const initWithDbRecord = (userInfo: any, roles: UserRole[]): IUserFull => {
  const user: IUserFull = userInfo;
  user.roles = roles;
  if (userInfo.settings) {
    user.settings = JSON.parse(userInfo.settings);
  }
  if (userInfo.providerData) {
    user.providerData = JSON.parse(userInfo.providerData);
  }
  return user;
};
