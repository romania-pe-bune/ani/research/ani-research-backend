import { ConnectionPool, IProcedureResult, MAX, Request as SqlRequest, TYPES } from 'mssql';
import { initWithDbRecord, IProviderData, IUser, IUserFull, UserStatus } from '~entities';
import { sqlNVarChar, sqlVarChar } from '~shared';


/**
 * User Data Access
 */
export class UserDao {
  private sql: ConnectionPool;

  /**
   * @param {ConnectionPool} pool Connection pool to use
   */
  constructor(pool: ConnectionPool) {
    this.sql = pool;
  }

  /**
   * Get user by id
   * @param {number} id
   * @return {Promise<IUserFull>}
   */
  public async getById(id: number): Promise<IUserFull> {
    try {
      const result = await new SqlRequest(this.sql)
        .input('id', TYPES.Int, id)
        .execute('getUserById');
      const roles = result.recordsets[1].map((record) => record.roleId);
      return initWithDbRecord(result.recordset[0], roles);
    } catch (error) {
      throw error;
    }
  }

  /**
   * Get user by id
   * @param {number} id
   * @return {Promise<IUserFull>}
   */
  public async deserializeUser(id: number): Promise<IUserFull> {
    try {
      const result = await new SqlRequest(this.sql)
        .input('userId', TYPES.Int, id)
        .execute('deserializeUser');
      const roles = result.recordsets[1].map((record) => record.roleId);
      return initWithDbRecord(result.recordset[0], roles);
    } catch (error) {
      throw error;
    }
  }

  /**
   * @param {string} email
   * @return {Promise<IUserFull>}
   */
  public async getByEmail(email: string): Promise<IUserFull> {
    try {
      const result = await new SqlRequest(this.sql)
        .input('email', sqlVarChar(100), email)
        .execute('getUserByEmail');
      if (!result.recordset?.length) {
        return ((null as unknown) as IUserFull);
      }
      const roles = result.recordsets[1].map((record) => record.roleId);
      return initWithDbRecord(result.recordset[0], roles);
    } catch (error) {
      throw error;
    }
  }

  /**
   * @param {string} id
   * @return {Promise<IUserFull>}
   */
  public async getByGoogleId(id: string): Promise<IUserFull> {
    try {
      const result = await new SqlRequest(this.sql)
        .input('googleId', sqlVarChar(50), id)
        .execute('getUserByGoogleId');
      if (!result.recordset?.length) {
        return ((null as unknown) as IUserFull);
      }
      const roles = result.recordsets[1].map((record) => record.roleId);
      return initWithDbRecord(result.recordset[0], roles);
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param {IUserAddDTO} user
   */
  public async add(user: IUserAddDTO): Promise<IProcedureResult<any>> {
    try {
      const sqlReq = new SqlRequest(this.sql)
        .input('firstName', sqlNVarChar(100), user.firstName)
        .input('lastName', sqlNVarChar(100), user.lastName)
        .input('displayName', sqlNVarChar(200), user.displayName)
        .input('email', sqlVarChar(100), user.email)
        .input('provider', sqlVarChar(50), user.provider)
        .input('providerData', sqlVarChar(MAX), JSON.stringify(user.providerData))
        .input('googleId', sqlVarChar(50), user.googleId)
        .input('profileImageUrl', sqlVarChar(512), user.profileImageUrl);

      sqlReq.output('userId', TYPES.Int);

      const result = await sqlReq.execute('userAdd');
      return result;
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param {IUserFull} user
   */
  public async update(user: IUserFull): Promise<IProcedureResult<any>> {
    try {
      const sqlReq = new SqlRequest(this.sql)
        .input('id', TYPES.Int, user.id)
        .input('firstName', sqlNVarChar(100), user.firstName)
        .input('lastName', sqlNVarChar(100), user.lastName)
        .input('displayName', sqlNVarChar(200), user.displayName)
        .input('phone', sqlVarChar(50), user.phone)
        .input('status', TYPES.TinyInt, user.status)
        .input('roles', sqlVarChar(50), user.roles.join());

      const result = await sqlReq.execute('userUpdate');
      return result;
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param {number} userId
   * @param {UserStatus} status
   */
  public async updateStatus(userId: number, status: UserStatus): Promise<IProcedureResult<any>> {
    try {
      const sqlReq = new SqlRequest(this.sql)
        .input('id', TYPES.Int, userId)
        .input('status', TYPES.TinyInt, status);

      const result = await sqlReq.execute('userUpdateStatus');
      return result;
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param {number} id
   */
  public async delete(id: number): Promise<IProcedureResult<any>> {
    try {
      const sqlReq = new SqlRequest(this.sql)
        .input('id', TYPES.Int, id);

      const result = await sqlReq.execute('userDelete');
      return result;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Get user list
   * @param {boolean} deleted
   * @return {Promise<IUserFull>}
   */
  public async list(deleted: boolean): Promise<IUser[]> {
    try {
      const result = await new SqlRequest(this.sql)
        .input('deleted', TYPES.TinyInt, deleted ? 1 : 0)
        .execute('userList');
      return result.recordset;
    } catch (error) {
      throw error;
    }
  }
}


export interface IUserAddDTO {
  firstName: string;
  lastName: string;
  displayName: string;
  email: string;
  provider: string;
  providerData: IProviderData;
  profileImageUrl: string;
  googleId: string;
  status: UserStatus;
}
