import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import { ErrorResponse, ApiError } from '~entities';
import {
  logger, parseError, getRequestUser
} from '~shared';


/** User controller */
export class UserController {
  /**
   * Gets logged in user info
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<void>}
   */
  public async getMe(req: Request, res: Response): Promise<void> {
    try {
      const user = getRequestUser(req);
      if (!user) {
        res.status(StatusCodes.UNAUTHORIZED).json(new ErrorResponse(ApiError.invalid_access));
        return;
      }
      logger.debug(`user::me ${user.email}`);

      res.status(StatusCodes.OK).json({});
      // const sqlpool = await vmmapApp.sqlPool;
      // const userDao = new UserDao(sqlpool);
      // const userInfo: IUserFull = await userDao.deserializeUser(user.id);
      // if (userInfo) {
      //   const u: IUser = {
      //     id: userInfo.id,
      //     firstName: userInfo.firstName,
      //     lastName: userInfo.lastName,
      //     displayName: userInfo.displayName,
      //     created: userInfo.created,
      //     email: userInfo.email,
      //     companyId: userInfo.companyId,
      //     company: userInfo.company,
      //     roleId: userInfo.roleId,
      //     roles: getUserRoles(userInfo.roleIds),
      //     teamId: userInfo.teamId,
      //     legalConsent: userInfo.legalConsent,
      //     settings: userInfo.settings
      //   };

      //   res.status(StatusCodes.OK).json(u);
      // } else {
      //   res.status(StatusCodes.NOT_FOUND).json(new ErrorResponse(ApiError.not_found));
      // }
    } catch (ex) {
      logger.error(parseError(ex));
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(new ErrorResponse(ApiError.internal_error));
    }
  }

  /**
   * Updates user display name
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<void>}
   */
  public async update(req: Request, res: Response): Promise<void> {
    try {
      const user = getRequestUser(req);
      if (!user) {
        res.status(StatusCodes.UNAUTHORIZED).json(new ErrorResponse(ApiError.invalid_access));
        return;
      }
      logger.debug(`user::update ${user.email}`);

      user.firstName = req.body.firstName;
      user.lastName = req.body.lastName;
      user.displayName = `${user.firstName} ${user.lastName}`;

      // const sqlpool = await vmmapApp.sqlPool;
      // const userDao = new UserDao(sqlpool);
      // await userDao.update(user);

      res.status(StatusCodes.OK).json({});
    } catch (ex) {
      logger.error(parseError(ex));
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(new ErrorResponse(ApiError.internal_error));
    }
  }

  /**
   * Deletes user account
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<void>}
   */
  public async delete(req: Request, res: Response): Promise<void> {
    try {
      const user = getRequestUser(req);
      if (!user) {
        res.status(StatusCodes.UNAUTHORIZED).json(new ErrorResponse(ApiError.invalid_access));
        return;
      }
      logger.debug(`user::delete ${user.email}`);

      // await removeUser(user.id);

      res.status(StatusCodes.OK).json({});
    } catch (ex) {
      logger.error(parseError(ex));
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(new ErrorResponse(ApiError.internal_error));
    }
  }

  /**
   * Updates UI settings
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<void>}
   */
  public async updateUISettings(req: Request, res: Response): Promise<void> {
    try {
      const user = getRequestUser(req);
      if (!user) {
        res.status(StatusCodes.UNAUTHORIZED).json(new ErrorResponse(ApiError.invalid_access));
        return;
      }

      // const settings = JSON.stringify(req.body.settings || {});

      // const sqlpool = await vmmapApp.sqlPool;
      // const userDao = new UserDao(sqlpool);
      // await userDao.updateUISettings(user.id, settings);
      res.status(StatusCodes.OK).json(user);
    } catch (ex) {
      logger.error(parseError(ex));
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(new ErrorResponse(ApiError.internal_error));
    }
  }
}
