IF EXISTS (SELECT *
FROM sysobjects
WHERE type = 'P' AND name = 'userDelete')
	BEGIN
  DROP  Procedure  userDelete
END
GO

CREATE PROCEDURE [dbo].userDelete(
  @id INT
)
AS
BEGIN
  SET NOCOUNT ON

  -- BEGIN TRANSACTION

  UPDATE	[dbo].[User]
	SET	deleted = 1,
      updated = GETDATE()
	WHERE	id = @id

  IF @@ERROR <> 0
  BEGIN
    -- ROLLBACK TRANSACTION
    RETURN -1
  END

  -- DELETE FROM UserRoles WHERE userId = @id

  -- IF @@ERROR <> 0
  -- BEGIN
  --   ROLLBACK TRANSACTION
  --   RETURN -2
  -- END

  -- COMMIT TRANSACTION
END
GO
GRANT EXEC ON [dbo].userDelete TO rpb_role
GO